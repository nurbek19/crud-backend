const express = require('express');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = db => {
    router.get('/', (req, res) => {
        db.query('SELECT `id`, `title`, `image`, `publication_date` FROM `news`', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    router.get('/:id', (req, res) => {
        db.query('SELECT * FROM `news` WHERE id=' + req.params.id, function (error, results) {
            if (error) throw error;

            res.send(results[0]);
        });
    });

    router.post('/', upload.single('image'), (req, res) => {
        const news = req.body;
        news.publication_date = new Date().toISOString();

        if(req.file) {
            news.image = req.file.filename;
        } else {
            news.image = null;
        }

        db.query(
            'INSERT INTO `news` (`title`, `description`, `image`, `publication_date`) ' +
            'VALUES (?, ?, ?, ?)',
            [news.title, news.description, news.image, news.publication_date],
            (error, results) => {
                if (error) throw error;

                news.id = results.insertId;
                res.send(news);
            }
        );
    });

    router.delete('/:id', (req, res) => {
        db.query('DELETE FROM `news` WHERE id=' + req.params.id, function (error, results) {
            if (error) throw error;

            res.send('Successfully deleted');
        });
    });

    return router;
};

module.exports = createRouter;