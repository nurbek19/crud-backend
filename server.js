const express = require('express');
const news = require('./app/news');
const comments = require('./app/comments');
const mysql = require('mysql');
const cors = require('cors');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'news_db'
});


connection.connect((err) => {
    if (err) throw err;

    app.use('/news', news(connection));
    app.use('/comments', comments(connection));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});